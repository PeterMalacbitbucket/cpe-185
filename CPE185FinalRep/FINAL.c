#include "simpletools.h"                      // Libraries included
#include "sirc.h"
#include "adcDCpropab.h"
#include <propeller.h>
#include "ws2812.h"
// led chain
#define LED_PIN     13
#define LED_COUNT   6

void background();  //prototype
                    
// 4 Parallax WS2812B Fun Boards
uint32_t ledColors[LED_COUNT];

// LED driver state
ws2812 *driver;

uint32_t pattern[] = {
    COLOR_RED,
    COLOR_ORANGE,
    COLOR_YELLOW, 
    COLOR_GREEN,
    COLOR_BLUE,
    COLOR_INDIGO
};
// ticks per millisecond
#define pattern_count  (sizeof(pattern) / sizeof(pattern[0]))
int ticks_per_ms;
int milliseconds;
int msTicks;
int tSync;

// forward declarations
void alternate(int count, int delay);
void chase(int count, int delay);
void pause(int ms);
                                        //  decoding signal from P10
void main(void)
{
    // calibrate the pause function
    // CLKFREQ is the clock frequency of the processor
    // typically this is 80mhz
    // dividing by 1000 gives the number of clock ticks per millisecond
    ticks_per_ms = CLKFREQ / 100;
    sirc_setTimeout(1000);                      // -1 if no remote code in 1 s
    // load the LED driver
    float ad1, ad2;
    float cm = 40;
    float time1, time2;
    adc_init(21, 20, 19, 18);                   // Initialize ADC on Activity Board
    milliseconds = 0;
    cog_run(background, 128);
    if (!(driver = ws2812b_open()))
        return;
    // repeat the patterns
    while(1) {
      adc_init(21, 20, 19, 18);
      ad1 = adc_volts(1);                      
      ad2 = adc_volts(2);
      int pin = adc_volts(0);
      
      putChar(HOME);                            // Set cursor to top left
      print("Reading: %f%c\n", ad1, CLREOL);    // Display voltage from sensor
      print("Reading: %f%c\n", ad2, CLREOL);
      
      if(ad1 > 2 & ad1 < 3){
        time1 = milliseconds;
      }
      if(ad2 > 2 & ad2 < 3){
        time2 = milliseconds;
      }  
      print("Current time (ms): %d %c\n", milliseconds,CLREOL); 
      print("Time1 (ms): %d %c\n", time1,CLREOL);
      print("Time2 (ms): %d %c\n", time2,CLREOL);  
      print("Speed (cm/s): %f%c\n", 1000*(cm/(time2-time1)), CLREOL);
    
      WEpassTranstoStop();
      if(pin > 1){
        pause(2000);
       }
       NSpassTranstoStop();           
    }
    // close the driver
    ws2812_close(driver);
}
void background(){
  msTicks = ((CLKFREQ) / 1000);
  tSync = (CNT);
  while (1) {
    tSync = (tSync + msTicks);
    waitcnt(tSync);
    milliseconds = (milliseconds + 1);
  }
}
void NSpassTranstoStop(void){
    
        ledColors[0] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0x001600;
       
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0;        
        ledColors[1] = 0x161600;   //YELLOW LOW BRIGHTNESS
        ledColors[2] = 0;
        
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(250);
        
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0x001600;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
       // WEpassTranstoStop();
} 

void WEpassTranstoStop(void){
    
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0;
       
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0x001600;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0x160000;  //YELLOW LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0x161600;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(250);
        
        ledColors[0] = 0;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0x001600;
        
        ledColors[3] = 0x160000;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
}                    

void pause(int ms)
{
    waitcnt(CNT + ms * ticks_per_ms);
}
    
void remote(){
    int button = sirc_button(10);
    
    switch(button){
      
      case 21 : 
        print("%c Remote Button = TV Power%c",HOME, CLREOL);
        break;
      case 18 : 
          print("%c Remote Button = Volume +%c",HOME, CLREOL);
          break;
      case 19 : 
          print("%c Remote Button = Volume -%c",HOME, CLREOL);
          break;    
      case 16 : 
          print("%c Remote Button = Channel +%c",HOME, CLREOL);
          break;    
      case 17 : 
          print("%c Remote Button = Channel -%c",HOME, CLREOL);
          break;
      case 20 : 
          print("%c Remote Button = Mute%c",HOME, CLREOL);
          break; 
      case 37 : 
          print("%c Remote Button = Input%c",HOME, CLREOL);
          break;    
      case 54 : 
          print("%c Remote Button = Asterisk(*)%c",HOME, CLREOL);
          break;    
      case 96 : 
          print("%c Remote Button = Menu%c",HOME, CLREOL);
          break;    
      case 101 : 
          print("%c Remote Button = OK%c",HOME, CLREOL);
          break;
      case 116 : 
          print("%c Remote Button = D-Pad Up%c",HOME, CLREOL);
          break;  
      case 117 : 
          print("%c Remote Button = D-Pad Down%c",HOME, CLREOL);
          break;  
      case 52 : 
          print("%c Remote Button = D-Pad Left%c",HOME, CLREOL);
          break;
      case 51 : 
          print("%c Remote Button = D-Pad Right%c",HOME, CLREOL);
          break;    
      case 27 : 
          print("%c Remote Button = Reverse%c",HOME, CLREOL);
          break;                
      case 26 : 
          print("%c Remote Button = Play%c",HOME, CLREOL);
          break;
      case 25 : 
          print("%c Remote Button = Pause%c",HOME, CLREOL);
          break;
      case 28 : 
          print("%c Remote Button = Fast Forward%c",HOME, CLREOL);
          break;
      case 32 : 
          print("%c Remote Button = Record%c",HOME, CLREOL);
          break;
      case 24 : 
          print("%c Remote Button = Stop%c",HOME, CLREOL);
          break;
      case 0 : 
          print("%c Remote Button = Num Pad 0%c",HOME, CLREOL);
          break;
      case 1 : 
          print("%c Remote Button = Num Pad 1%c",HOME, CLREOL);
          break;
      case 2 : 
          print("%c Remote Button = Num Pad 2%c",HOME, CLREOL);
          break;
     }
  }           